﻿import java.util.*;

class Fahrkartenautomat
{
	
	
	public static double centZuEuro(int cent) {
		double euro = cent;
		euro = euro / 100;
		return euro;
	}
	
	public static int fahrkartenbestellungErfassen() {
		
		Scanner tickets = new Scanner(System.in);
		String fahrkarte[] = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
				"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
		int kartenpreisCent[] = {290, 330, 360, 190, 860, 900, 960, 2350, 2430, 2490};
		int arrayLength = kartenpreisCent.length;
		int zuZahlenderBetragCent = 0;
		int ticketArt = 0;
		int eingabe = 0;
		
		System.out.printf("Fahrkartenbestellvorgang:\n");
		System.out.printf("=========================\n\n");
		
		while(ticketArt != arrayLength+1) {
			System.out.printf(" Wählen Sie :\n");
			int i = 0;
			for(i = 0; i < arrayLength; i++) {
				System.out.printf("%8s %-35s %7.2f €\n", "("+(i+1)+")", fahrkarte[i], centZuEuro(kartenpreisCent[i]));
			}
			System.out.printf("\n%8s Bezahlen\n", "("+(arrayLength+1)+")");
			System.out.printf("\n Ihre Wahl : ");
			try {
				ticketArt = tickets.nextInt();
			} catch(InputMismatchException e) {
				String error = tickets.next();
				System.out.printf("\n" + error + " ist keine gültige Eingabe\n");
				ticketArt = 0;
				eingabe = -1;
			}
			System.out.printf("\n\n");
			if(eingabe != -1) {
				if(ticketArt > 0 && ticketArt < arrayLength+1) {
					System.out.printf("Wie viele Tickets möchten Sie kaufen : ");
					int anzahlTickets = tickets.nextInt();
					System.out.printf("\n\n");
					zuZahlenderBetragCent += (anzahlTickets*kartenpreisCent[ticketArt-1]);
				} else if(ticketArt == arrayLength+1) {
					
				} else {
					System.out.printf(ticketArt + " ist keine gültige Eingabe\n\n\n");
				}
			} else {
				eingabe = 0;
			}
		}
		
		return zuZahlenderBetragCent;
	}
	
	
	public static int fahrkartenBezahlen(int zuZahlenderBetragCent) {
		
		int eingezahlterGesamtbetragCent = 000;
		double eingezahlterGesamtbetrag = 0;
		double zuZahlenderBetrag = centZuEuro(zuZahlenderBetragCent);
		double eingeworfeneMünze;
		int eingeworfeneMünzeCent;
		
		Scanner tastatur = new Scanner(System.in);
		
		while(eingezahlterGesamtbetragCent < zuZahlenderBetragCent)
	       {
	    	   System.out.printf("Noch zu zahlen: " + "%.2f" + " Euro\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
	    	   System.out.print("Eingabe (mind. 1Ct, höchstens 10 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	    	   eingeworfeneMünzeCent = (int)(eingeworfeneMünze * 100);
	           eingezahlterGesamtbetragCent += eingeworfeneMünzeCent;
	           zuZahlenderBetrag -= eingeworfeneMünze;
	       }
		
		return eingezahlterGesamtbetragCent;
		
	}
	
	public static void fahrkartenAusgabe() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++) {
	          System.out.print("=");
	          warte(250);
	       }
	       System.out.println("\n\n");
	}
	
	public static void rückgeldAusgabe(int eingezahlterGesamtbetragCent, int zuZahlenderBetragCent) {
		int rückgabebetragCent = eingezahlterGesamtbetragCent - zuZahlenderBetragCent;
	    double rückgabebetrag = rückgabebetragCent;
	    rückgabebetrag = rückgabebetrag / 100;
	    if(rückgabebetragCent > 000)
	    {
	       System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" + " EURO\n", rückgabebetrag);
	       System.out.println("wird in folgenden Münzen ausgezahlt:");
	       warte(1000);

	       while(rückgabebetragCent >= 200) { 		// 2 EURO-Münzen
	    	   rückgabebetragCent = muenzeAusgeben(200, rückgabebetragCent);
	       }
	       
	       while(rückgabebetragCent >= 100) {		// 1 EURO-Münzen
	    	   rückgabebetragCent = muenzeAusgeben(100, rückgabebetragCent);
	       }
	       
	       while(rückgabebetragCent >= 50) {		// 50 CENT-Münzen
	    	   rückgabebetragCent = muenzeAusgeben(50, rückgabebetragCent);
	       }
	       
	       while(rückgabebetragCent >= 20) {		// 20 CENT-Münzen
	    	   rückgabebetragCent = muenzeAusgeben(20, rückgabebetragCent);
	       }
	       
	       while(rückgabebetragCent >= 10) {		// 10 CENT-Münzen
	    	   rückgabebetragCent = muenzeAusgeben(10, rückgabebetragCent);
	       }
	       
	       while(rückgabebetragCent >= 5) {			// 5 CENT-Münzen
	    	   rückgabebetragCent = muenzeAusgeben(5, rückgabebetragCent);
	       }
	       
	       while(rückgabebetragCent >= 2) {			// 2 CENT-Münzen
	    	   rückgabebetragCent = muenzeAusgeben(2, rückgabebetragCent);
	       }
	       
	       while(rückgabebetragCent >= 1) {			// 1 CENT-Münzen
	       	   rückgabebetragCent = muenzeAusgeben(1, rückgabebetragCent);
	       }
	   }
	}
	
	public static int muenzeAusgeben(int i, int rückgeld) {
		if(i >= 100) {
			System.out.printf("%d EURO\n", i / 100);
			rückgeld -= i;
		} else {
			System.out.printf("%d CENT\n", i);
			rückgeld -= i;
		}
		warte(600);
		return rückgeld;
	}
	
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
          }
          catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
          }
	}
	
	
    public static void main(String[] args)
    {
    	while(true == true) {
	    	int gesamtBetrag = fahrkartenbestellungErfassen();
	
	    	int gesamtEingezahlt = fahrkartenBezahlen(gesamtBetrag);
	    	    	
	    	fahrkartenAusgabe();
	    	
	    	rückgeldAusgabe(gesamtEingezahlt, gesamtBetrag);
	    	
	    	warte(1500);
	
	    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	    			"vor Fahrtantritt entwerten zu lassen!\n"+
	    			"Wir wünschen Ihnen eine gute Fahrt.");
	    	System.out.printf("========\n\n");
	    	warte(6000);
    	}
    }
}