package Angestellter;

public class Angestellter {

	 private String name;
	 private double gehalt;
	 //TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
	 private String vorname;
	 
	 //TODO: 4. Implementieren Sie einen Konstruktor, der alle Attribute initialisiert.
	 Angestellter(String vorname, String name, double gehalt) {
		 setName(name);
		 setVorname(vorname);
		 setGehalt(gehalt);
	 }
	 
	 //TODO: 5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen initialisiert.
	 Angestellter(String vorname, String name) {
		 this.name = name;
		 this.vorname = vorname;
	 }

	 public void setName(String name) {
		 this.name = name;
	 }
	 
	 public String getName() {
		 return this.name;
	 }
	 
	 public void setVorname(String vorname) {
		 this.vorname = vorname;
	 }
	 
	 public String getVorname() {
		 return this.vorname;
	 }

	 public void setGehalt(double gehalt) {
	   //TODO: 1. Implementieren Sie die entsprechende set-Methoden. 
	   //Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		 if (gehalt >= 0.0) {
			 this.gehalt = gehalt;
		 } else {
			 System.out.println("Gehalt darf nicht negativ sein.");
		 }
	 }
 
	 public double getGehalt() {
	   //TODO: 2. Implementieren Sie die entsprechende get-Methoden.
		 return this.gehalt;
	 }
	 
	 //TODO: 6. Implementieren Sie eine Methode 'vollname', die den vollen Namen (Vor- und Zuname) als string zur�ckgibt.
	 public String vollname() {
		 return this.vorname + " " + this.name;
	 }
}
