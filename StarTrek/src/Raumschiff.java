import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
	
	private String name;
	private int energieProzent;
	private int schutzschildeProzent;
	private int lebenserhaltungProzent;
	private int huelleProzent;
	private int anzahlTorpedos;
	private int anzahlRepAndroiden;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladeliste = new ArrayList<Ladung>();
	
	
	Raumschiff() {
		
	}
	
	Raumschiff(String name, int energieProzent, int schutzschildeProzent, int lebenserhaltungProzent, int huelleProzent, int anzahlTorpedos, int anzahlRepAndroiden) {
		this.name = name;
		this.energieProzent = energieProzent;
		this.schutzschildeProzent = schutzschildeProzent;
		this.lebenserhaltungProzent = lebenserhaltungProzent;
		this.huelleProzent = huelleProzent;
		this.anzahlTorpedos = anzahlTorpedos;
		this.anzahlRepAndroiden = anzahlRepAndroiden;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setEnergieProzent(int energieProzent) {
		this.energieProzent = energieProzent;
	}
	
	public int getEnergieProzent() {
		return this.energieProzent;
	}
	
	public void setSchutzschildeProzent(int schutzschildeProzent) {
		this.schutzschildeProzent = schutzschildeProzent;
	}
	
	public int getSchutzschildeProzent() {
		return this.schutzschildeProzent;
	}
	
	public void setLebenserhaltungProzent(int lebenserhaltungProzent) {
		this.lebenserhaltungProzent = lebenserhaltungProzent;
	}
	
	public int getLebenserhaltungProzent() {
		return this.lebenserhaltungProzent;
	}
	
	public void setHuelleProzent(int huelleProzent) {
		this.huelleProzent = huelleProzent;
	}
	
	public int getHuelleProzent() {
		return this.huelleProzent;
	}
	
	public void setAnzahlTorpedos(int anzahlTorpedos) {
		this.anzahlTorpedos = anzahlTorpedos;
	}
	
	public int getAnzahlTorpedos() {
		return this.anzahlTorpedos;
	}
	
	public void setAnzahlRepAndroiden(int anzahlRepAndroiden) {
		this.anzahlRepAndroiden = anzahlRepAndroiden;
	}
	
	public int getAnzahlRepAndroiden() {
		return this.anzahlRepAndroiden;
	}
	
	public void addLadung(String bez, double menge) {
		Ladung nextLadung = new Ladung(bez, menge);
		ladeliste.add(nextLadung);
	}
	
	public void schiffszustand() {
		System.out.printf("%-20s %7s\n", "Schiffsname:", this.name);
		System.out.printf("%-20s %4s%%\n", "Energie:", this.energieProzent);
		System.out.printf("%-20s %4s%%\n", "Schilde:", this.schutzschildeProzent);
		System.out.printf("%-20s %4s%%\n", "Lebenserhaltung:", this.lebenserhaltungProzent);
		System.out.printf("%-20s %4s%%\n", "H�lle:", this.huelleProzent);
		System.out.printf("%-20s %5s\n", "Torpedos:", this.anzahlTorpedos);
		System.out.printf("%-20s %5s\n\n", "Reparatur-Androiden:", this.anzahlRepAndroiden);
	}
	
	public void ausgabeLadeliste() {
		System.out.println("Ladeliste:");
		for(Ladung tmp : ladeliste) {
			System.out.println(tmp.getBezeichnung());
			System.out.println(tmp.getMenge());
			System.out.println();
		}
	}
	
	public void schiesseTorpedo(Raumschiff ziel) {
		if(ziel.getLebenserhaltungProzent() > 0) {
			if(this.getAnzahlTorpedos() == 0) {
				System.out.printf("-=*Click*=-\n\n");
			} else {
				this.setAnzahlTorpedos(this.getAnzahlTorpedos()-1);
				nachrichtAnAlle("Torpedo abgeschossen");
				istGetroffen(ziel);
			}
		} else {
			System.out.printf("Kein g�ltiges Ziel\n\n");
		}
	}
	
	public void schiesseKanone(Raumschiff ziel) {
		if(ziel.getLebenserhaltungProzent() > 0) {
			if(this.getEnergieProzent() < 50) {
				System.out.printf("-=*Click*=-\n\n");
			} else {
				this.setEnergieProzent(this.getEnergieProzent()-50);
				nachrichtAnAlle("Kanone abgeschossen");
				istGetroffen(ziel);
			}
		} else {
			System.out.printf("Kein g�ltiges Ziel\n\n");
		}
	}
	
	private void istGetroffen(Raumschiff ziel) {
		System.out.printf("%s wurde getroffen!\n\n", ziel.getName());
		if(ziel.getSchutzschildeProzent() != 0) {
			ziel.setSchutzschildeProzent(ziel.getSchutzschildeProzent()-50);
		} else {
			ziel.setHuelleProzent(ziel.getHuelleProzent()-50);
			if(ziel.getEnergieProzent() >= 50) {
				ziel.setEnergieProzent(ziel.getEnergieProzent()-50);
			}
		}
		if(ziel.getHuelleProzent() <= 0) {
			ziel.setLebenserhaltungProzent(0);
			nachrichtAnAlle("Lebenserhaltungssysteme vernichtet");
		}
	}
	
	public static void nachrichtAnAlle(String msg) {
		broadcastKommunikator.add(msg);
	}
	
	public static void ausgabeLogbuecher() {
		System.out.println("Broadcast Nachrichten:");
		for(String tmp : broadcastKommunikator) {
			System.out.println(tmp);
		}
		System.out.println();
	}
	
	public void ladelisteAufraeumen() {
		ArrayList<Integer> indexLoeschen = new ArrayList<Integer>();
		for(Ladung tmp : ladeliste) {
			if(tmp.getMenge() == 0) {
				indexLoeschen.add(ladeliste.indexOf(tmp));
			}
		}
		for(int i : indexLoeschen) {
			ladeliste.remove(i);
		}
	}
	
	public void torpedosNachladen(int anzahl) {
		for(Ladung tmp : ladeliste) {
			if(tmp.getBezeichnung() == "torpedos") {
				if(tmp.getMenge() < anzahl) {
					anzahl = (int)tmp.getMenge();
				}
				this.setAnzahlTorpedos(this.getAnzahlTorpedos()+anzahl);
				tmp.setMenge(tmp.getMenge()-anzahl);
				System.out.printf("%d Torpedo(s) eingesetzt\n", anzahl); 
			} else {
				System.out.println("Keine Torpedos gefunden");
				nachrichtAnAlle("-=*Click*=-");
			}
		}
	}
	
	public void reparatur(boolean energie, boolean schilde, boolean huelle, int androiden) {
		int rep = 0;
		if(energie) {
			rep++;
		}
		if(schilde) {
			rep++;
		}
		if(huelle) {
			rep++;
		}
		int rng;
		Random random = new Random();
		rng = random.nextInt(101);
		if(androiden > this.getAnzahlRepAndroiden()) {
			androiden = this.getAnzahlRepAndroiden();
		}
		int sum = (rng*androiden)/rep;
		if(energie) {
			this.setEnergieProzent(this.getEnergieProzent()+sum);
		}
		if(schilde) {
			this.setSchutzschildeProzent(this.getSchutzschildeProzent()+sum);
		}
		if(huelle) {
			this.setHuelleProzent(this.getHuelleProzent()+sum);
		}
		this.setAnzahlRepAndroiden(this.getAnzahlRepAndroiden()-androiden);
	}
	
}
