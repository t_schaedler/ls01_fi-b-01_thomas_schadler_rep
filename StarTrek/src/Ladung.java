
public class Ladung {
	
	private String bezeichnung;
	private double menge;
	
	
	Ladung() {
		
	}
	
	Ladung(String bezeichnung, double menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	public void setBezeichnung(String bez) {
		this.bezeichnung = bez;
	}
	
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	public void setMenge(double menge) {
		this.menge = menge;
	}
	
	public double getMenge() {
		return this.menge;
	}
	
}
